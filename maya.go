package main

import (
	"fmt"
	"time"
)

var BlackfootMonths = []string{"Frost in the Tepee",
	"Dark Red Calves",
	"Snowblind",
	"Grass Appearing",
	"Shedding Ponies",
	"Making of Fat",
	"Ripe Cherries",
	"Black Cherries",
	"Scarlet Plums",
	"Changing Season",
	"Falling Leaves",
	"Popping Trees"}

var MayanDays = []string{"Imix", "Ik", "Akbal", "Kan", "Chicchan", "Cimi", "Manik", "Lamat", "Muluc", "Oc", "Chuen", "Eb", "Ben", "Ix", "Men", "Cib", "Caban", "Etznab", "Cauac", "Ahau"}
var MayanMonths = []string{"Pop", "Uo", "Zip", "Zotz", "Tzec", "Xuc", "Yaxkin", "Mol", "Chen", "Yax", "Zac", "Ceh", "Mac", "Kankin", "Muan", "Pax", "Kayab", "Cumku", "Uyabeb"}

func correx(date float32) float32 {
	year := date * 100 / 36525
	k := date - year*36525/100
	if k < 0 {
		k += 365
	}
	j := 59
	if int(year)%4 == 0.0 {
		j = 60
	}
	if int(k) < j {
		year--

	}
	year = (year - 5012) / 100
	ctemp := year*3/4 - 1
	if year > 0 {
		ctemp += 2
	}
	return float32(ctemp)

}

func julian(d time.Time) float32 {

	year := d.Year() + 4712
	j1 := float32(365*year + (year / 4))
	month := d.Month()
	c := 30.6*float32(month) - 32.3

	if month < 3 {
		if year%4 == 0.0 {
			j1--
		}
		c = c + 2.3
	}

	day := float32(d.Day())
	jtemp := j1 + c + day
	jtemp = jtemp - correx(jtemp)

	return jtemp

}

const GMTC = 584283

func maya(d time.Time) []int {
	date := d.Year() - GMTC
	if date > 1872000 {
		date = date % 1872000
	}
	if date < 0 {
		date = 1872000 + date%1872000
	}

	round := make([]int, 5)
	round[0] = date / 144000
	date = date - round[0]*144000
	round[1] = date / 7200
	date = date - round[1]*7200
	round[2] = date / 360
	date = date - round[2]*360
	round[3] = date / 20
	round[4] = date - round[3]*20

	return round

}

func cround(juldate int) string {

	tdate := juldate - GMTC
	mm := 348
	if tdate <= 0 {
		mm = mm + 280*(tdate/1872000-1)
		tdate = 1872000 + tdate%1872000
	}
	dval := make([]int, 4)
	dval[0] = (tdate + 4) % 13
	if dval[0] == 0 {
		dval[0] = 13
	}

	dval[1] = tdate % 20
	if dval[1] == 0 {
		dval[1] = 20
	}

	dval[3] = (tdate + mm) % 365
	if dval[3] == 0 {
		dval[3] = 365
	}
	dval[2] = dval[3] % 20
	dval[3] = dval[3]/20 + 1
	if dval[2] == 0 {
		dval[2] = 20
		dval[3]--
	}

	return fmt.Sprintf("The Calendar Round position is %d %s %d %s.\n",
		dval[0], MayanDays[dval[1]-1], dval[2], MayanMonths[dval[3]-1])

}

func main() {
	now := time.Now()
	na_month := BlackfootMonths[now.Month()]

	fmt.Printf("The calendar date is %s.\nIt is the Blackfoot Month of the %s\n", now.Format("02 Jan 2006"), na_month)

	jdate := julian(now)
	fmt.Printf("The Julian date is %f.\n", jdate)

	mdate := maya(now)
	fmt.Printf("The Mayan int count is %d . %d . %d . %d . %d.\n", mdate[0], mdate[1], mdate[2], mdate[3], mdate[4])

	the_round := cround(int(jdate))
	fmt.Printf("%s", the_round)

	fmt.Printf("\nAt the tone, the time will be: %s\n", now.Format(time.TimeOnly))

}
