pub mod maya {
    use chrono::prelude::*;

    /// Converts from a Gregorian data to a Julian date using the formula from
    /// [Converting Between Julian Dates and Gregorian Calendar Dates](<https://aa.usno.navy.mil/faq/JD_formula>).
    ///
    /// JD =  367   K - <(7   (K + <(M + 9)  / 12>)) / 4> + <(275   M) / 9> + I + 1721013.5 + UT1/24 - 0.5sign(100K+M-190002.5) + 0.5
    /// where:
    ///
    /// - K is the year (1801 <= K <= 2099)
    /// - M is the month (1 <= M <= 12)
    /// - I is the day of the month (1 <= I <= 31)
    /// - UT1 is the universal time in hours
    ///
    /// from [Converting Between Julian Dates and Gregorian Calendar Dates](<https://aa.usno.navy.mil/faq/JD_formula>)
    pub fn julian(utc: DateTime<Utc>) -> f64 {
        let k = utc.year() as f64;
        let m = utc.month() as f64;
        let i = utc.day() as f64;
        let ut1 = utc.hour() as f64 + utc.minute() as f64 / 60. + utc.second() as f64 / 86_000.;
        let year_frac = ((m + 9.) / 12.).trunc();
        (367. * k - (7. * (k + year_frac) / 4.).trunc() + ((275. * m) / 9.).trunc() + i)
            + 1721013.5
            + ut1 / 24.
            - 0.5 * ((100. * k + m) - 190002.5).signum()
            + 0.5
    }

    pub const GMTC: i32 = 584283;

    const MAYAN_DAYS: [&str; 20] = [
        "Imix", "Ik", "Akbal", "Kan", "Chicchan", "Cimi", "Manik", "Lamat", "Muluc", "Oc", "Chuen",
        "Eb", "Ben", "Ix", "Men", "Cib", "Caban", "Etznab", "Cauac", "Ahau",
    ];
    const MAYAN_MONTHS: [&str; 19] = [
        "Pop", "Uo", "Zip", "Zotz", "Tzec", "Xuc", "Yaxkin", "Mol", "Chen", "Yax", "Zac", "Ceh",
        "Mac", "Kankin", "Muan", "Pax", "Kayab", "Cumku", "Uyabeb",
    ];

    pub fn cround(juldate: i32) -> Vec<String> {
        let mut days = Vec::new();
        let mut tdate = juldate - GMTC;
        let mut mm = 348;
        if tdate <= 0 {
            mm += 280 * (tdate / 1872000 - 1);
            tdate = 1872000 + tdate % 1872000;
        }
        let mut dv0 = (tdate + 4) % 13;
        if dv0 == 0 {
            dv0 = 13;
        }
        days.push(dv0.to_string());

        let mut dv1 = tdate % 20;
        if dv1 == 0 {
            dv1 = 20;
        }
        days.push(MAYAN_DAYS[(dv1 - 1) as usize].to_string());

        let mut d3 = (tdate + mm) % 365;
        if d3 == 0 {
            d3 = 365;
        }

        let mut d2 = d3 % 20 - 1;
        if d2 <= 0 {
            d2 = 20;
        }
        d3 = d3 / 20 + 1;
        if d2 == 0 {
            d2 = 20;
            d3 -= 1;
        }
        days.push((d2).to_string());
        days.push(MAYAN_MONTHS[(d3 - 1) as usize].to_string());
        days
    }

    pub fn maya(d: DateTime<Utc>) -> Vec<i32> {
        let mut date = d.year() - GMTC;
        if date > 1872000 {
            date %= 1872000;
        }
        if date < 0 {
            date = 1872000 + date % 1872000;
        }
        let mut round = Vec::with_capacity(5);
        round.push(&date / 144000); // 0
        date -= round[0] * 144000;
        round.push(date / 7200); // 1
        date -= round[1] * 7200;
        round.push(date / 360); // 2
        date -= round[2] * 360;
        round.push(date / 20); // 3
        round.push(date - round[3] * 20); // 4
        round
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn future() {
            let then = Utc.with_ymd_and_hms(2038, 1, 19, 3, 14, 7);
            let result = julian(then.unwrap());
            assert_eq!(result, 2465442.6347256135);
        }
        #[test]
        fn birthday() {
            let then = Utc.with_ymd_and_hms(1964, 6, 30, 0, 0, 0);
            let result = julian(then.unwrap());
            assert_eq!(result, 2438576.5);
        }

        #[test]
        fn calendar_round() {
            let then = Utc.with_ymd_and_hms(1964, 6, 30, 0, 0, 0);
            let result = julian(then.unwrap());
            assert_eq!(result, 2438576.5);
            let bd_round = cround(result.round() as i32);
            assert_eq!(bd_round.len(), 4);
            // 4 Ix 17 Sotz'
            assert_eq!(bd_round[0], "4");
            assert_eq!(bd_round[1], "Ix");
            assert_eq!(bd_round[2], "16");
            assert_eq!(bd_round[3], "Zotz");
        }

        #[test]
        fn long_count() {
            let then = Utc.with_ymd_and_hms(1964, 6, 30, 0, 0, 0);
            let result = maya(then.unwrap());
            assert_eq!(result.len(), 5);
        }
    }
}
