use chrono::prelude::*;
use rs_maya::maya::*;

const BLACKFOOT_MONTHS: [&str; 12] = [
    "Frost in the Tepee",
    "Dark Red Calves",
    "Snowblind",
    "Grass Appearing",
    "Shedding Ponies",
    "Making of Fat",
    "Ripe Cherries",
    "Black Cherries",
    "Scarlet Plums",
    "Changing Season",
    "Falling Leaves",
    "Popping Trees",
];

fn main() {
    println!("Hello, world!");

    let now = Local::now();
    println!(
        "The calendar date is {}.\nIt is the Blackfoot Month of the {}\n",
        now.format("%d %b %Y"),
        BLACKFOOT_MONTHS[now.month0() as usize]
    );
    let utc = Utc::now();
    println!("The time (UTC) is now {}!", utc);
    let jdate = julian(utc);
    println!("The Julian date is {:.5}", jdate);
    let count = maya(utc);
    println!(
        "The Mayan long count is {} . {} . {} . {} . {}",
        count[0], count[1], count[2], count[3], count[4]
    );
    let cr = cround(jdate as i32);
    println!(
        "The Calendar Round position is {} {} {} {}\n",
        cr[0], cr[1], cr[2], cr[3]
    );
    println!("At the tone, the time will be: {}", now.format("%H:%M:%S"));
    /*
    The Calendar Round position is 8 Oc 13 Zotz.

    At the tone, the time will be: 19:21:38
    */
}
